/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.doublylinkedlist;

/**
 *
 * @author Tauru
 */
class DoublyLinkedList {

    private Link2 first; // Reference to the first item
    private Link2 last; // Reference to the last item

    public DoublyLinkedList() {
        first = null; // No items in the list yet
        last = null;
    }

    public boolean isEmpty() {
        return first == null; // True if the list is empty
    }

    public void insertFirst(long dd) {
        Link2 newLink = new Link2(dd); // Create a new link
        if (isEmpty()) {
            last = newLink; // NewLink <-- last
        } else {
            first.previous = newLink; // NewLink <-- old first
        }
        newLink.next = first; // NewLink --> old first
        first = newLink; // first --> newLink
    }

    public void insertLast(long dd) {
        Link2 newLink = new Link2(dd); // Create a new link
        if (isEmpty()) {
            first = newLink; // first --> newLink
        } else {
            last.next = newLink; // old last --> newLink
            newLink.previous = last; // old last <-- newLink
        }
        last = newLink; // newLink <-- last
    }

    public Link2 deleteFirst() {
        Link2 temp = first; // (Assumes non-empty list)
        if (first.next == null) {
            last = null; // null <-- last
        } else {
            first.next.previous = null; // null <-- old next
        }
        first = first.next; // first --> old next
        return temp;
    }

    public Link2 deleteLast() {
        Link2 temp = last; // (Assumes non-empty list)
        if (first.next == null) {
            first = null; // first --> null
        } else {
            last.previous.next = null; // old previous --> null
        }
        last = last.previous; // old previous <-- last
        return temp;
    }

    public boolean insertAfter(long key, long dd) {
        Link2 current = first;
        while (current.dData != key) {
            current = current.next;
            if (current == null) {
                return false; // Didn't find it
            }
        }
        Link2 newLink = new Link2(dd);
        if (current == last) {
            newLink.next = null;
            last = newLink;
        } else {
            newLink.next = current.next;
            current.next.previous = newLink;
        }
        newLink.previous = current;
        current.next = newLink;
        return true;
    }

    public Link2 deleteKey(long key) {
        Link2 current = first;
        while (current.dData != key) {
            current = current.next;
            if (current == null) {
                return null; // Didn't find it
            }
        }
        if (current == first) {
            first = current.next;
        } else {
            current.previous.next = current.next;
        }
        if (current == last) {
            last = current.previous;
        } else {
            current.next.previous = current.previous;
        }
        return current;
    }

    public void displayForward() {
        System.out.print("List (first --> last): ");
        Link2 current = first;
        while (current != null) {
            current.displayLink();
            current = current.next;
        }
        System.out.println();
    }

    public void displayBackward() {
        System.out.print("List (last --> first): ");
        Link2 current = last;
        while (current != null) {
            current.displayLink();
            current = current.previous;
        }
        System.out.println();
    }
}

class Link {

    public long dData;
    public Link next;
    public Link previous;

    public Link(long dd) {
        dData = dd;
    }

    public void displayLink() {
        System.out.print(dData + " ");
    }
}
