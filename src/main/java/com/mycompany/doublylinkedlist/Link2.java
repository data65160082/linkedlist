/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.doublylinkedlist;

/**
 *
 * @author Tauru
 */
class Link2 {

    public long dData; // data item
    public Link2 next; // next link in list
    public Link2 previous; // previous link in list
// -------------------------------------------------------------

    public Link2(long d) // constructor
    {
        dData = d;
    }
// -------------------------------------------------------------

    public void displayLink() // display this link
    {
        System.out.print(dData + " ");
    }
// -------------------------------------------------------------
} // end class Link
////////////////////////////////////////////////////////////////
